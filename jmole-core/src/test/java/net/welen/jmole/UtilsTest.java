package net.welen.jmole;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.lang.management.ManagementFactory;

import javax.management.MBeanServerFactory;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UtilsTest {

	@Before
	public void setup() {
		MBeanServerFactory.createMBeanServer();
	}
	
	@Test
	public void testGetPlatformMBeanServer() {
		System.clearProperty(Utils.PROPERTY_MBEAN_SERVER);
		assertEquals(Utils.getMBeanServer(), ManagementFactory.getPlatformMBeanServer());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testGetNonExistingNonPlatformMBeanServer() {
		System.setProperty(Utils.PROPERTY_MBEAN_SERVER, "NonExisting");
		Utils.getMBeanServer();
	}

	@Test
	public void testGetNonPlatformMBeanServer() {
		System.setProperty(Utils.PROPERTY_MBEAN_SERVER, ".*");
		Utils.getMBeanServer();
	}

}
