package net.welen.jmole;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.management.MBeanServer; 
import javax.management.ObjectName;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class JMoleTest {

	private MBeanServer server = Utils.getMBeanServer(); 

	@Before
	public void setup() {
		System.setProperty(JMole.CONFIG_FILENAMES_PROPERTY, "src/test/resources/JMole_Test.xml");
		System.setProperty("JMOLE_TEST_LEVEL", "1");		
	}

	@Test	
	public void testMBeanRegistration() throws Exception {
		ObjectName objectName = new ObjectName(JMole.OBJECT_NAME);
		JMole jmole = new JMole();
		assertFalse(server.isRegistered(objectName));
		jmole.register();
		assertTrue(server.isRegistered(objectName));
		jmole.unregister();
		assertFalse(server.isRegistered(objectName));
	}

	@Test
	public void testMultipleMBeanRegistration() throws Exception {
		JMole jmole = new JMole();
		jmole.register();
		try {
			jmole.register();
		} catch (InstanceAlreadyExistsException e){
			// Do nothing. Expected
		} finally {
			jmole.unregister();
		}
	}

	@Test(expected=InstanceNotFoundException.class)
	public void testUnregistrationWithoutAnyMBean() throws Exception {
		new JMole().unregister();
	}

	@Test
	public void testConfiguration() throws Exception {
		JMole jmole = new JMole();
		assertEquals(0, jmole.getConfiguration().size());
		jmole.configure();
		assertEquals(1, jmole.getConfiguration().size());
	}

	@Test
	public void testXMLOverriddenConfiguration() throws Exception {
		String propString = JMole.CONFIG_FILENAME_XSLT_PROPERTY_PREFIX + "src/test/resources/JMole_Test.xml";
		try {
			System.setProperty(propString, "JMole_Test.xsl");
			JMole jmole = new JMole();
			assertEquals(0, jmole.getConfiguration().size());
			jmole.configure();
			assertEquals(0, jmole.getConfiguration().size());
		} finally {
			System.clearProperty(propString);
		}
	}

	@Test
	public void testConfigurationFromClasspath() throws Exception {		
		try {
			System.setProperty(JMole.CONFIG_FILENAMES_PROPERTY, "JMole_Test.xml");
			JMole jmole = new JMole();
			assertEquals(0, jmole.getConfiguration().size());
			jmole.configure();
			assertEquals(1, jmole.getConfiguration().size());
		} finally {
			setup();
		}
	}


	@Test
	public void testDiscovery() throws Exception {
		JMole jmole = new JMole();
		jmole.register();
		jmole.configure();
		assertEquals(1, jmole.getConfiguration().size());
		assertEquals(0, jmole.getConfiguration().iterator().next().getMBeanFinder().getMatchingObjectNames().size());

		jmole.discover();
		assertEquals(0, jmole.getConfiguration().iterator().next().getMBeanFinder().getMatchingObjectNames().size());

		ObjectName objectName = new ObjectName("net.welen.jmole.test:test=1");
		Mock mock = new Mock();
		mock.setAttribute("TestString");
		server.registerMBean(mock, objectName);
		assertEquals(1, jmole.getConfiguration().iterator().next().getMBeanFinder().getMatchingObjectNames().size());
		server.unregisterMBean(objectName);

		jmole.unregister();
	}

	@Test
	public void testCollectingValues() throws Exception {
		JMole jmole = new JMole();
        jmole.register();
		jmole.configure();

		ObjectName objectName = new ObjectName("net.welen.jmole.test:test=1");
	    Mock mock = new Mock();
	    mock.setAttribute("TestString");
		server.registerMBean(mock, objectName);

		assertEquals("Name TestString", jmole.getConfiguration().iterator().next().getMBeanCollector().getConstructedName(objectName));
		assertEquals("TestString", jmole.getConfiguration().iterator().next().getMBeanCollector().getValues(objectName).get("Attribute"));

		server.unregisterMBean(objectName);
		jmole.unregister();
	}

}	
