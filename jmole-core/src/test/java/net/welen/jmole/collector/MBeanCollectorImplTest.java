package net.welen.jmole.collector;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import net.welen.jmole.Utils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MBeanCollectorImplTest {
	
	private ObjectName objectName;
	private MBeanServer server;
	
	public static interface MyTestMBean {
		public int getX();
		public String getY();
	}
	
	public static class MyTest implements MyTestMBean {
		public int getX() {
			return 42;
		}
		public String getY() {
			return "YData";
		}
	}
	
	@Before
	public void setup() throws InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException, MalformedObjectNameException {
		objectName = new ObjectName("jmole.test:test=MBeanCollectorImpl,a=test");
		server = Utils.getMBeanServer(); 
		server.registerMBean(new MyTest(), objectName);
	}	
	
	@After
	public void cleanup() throws MBeanRegistrationException, InstanceNotFoundException {
		server.unregisterMBean(objectName);
	}	

	@Test
	public void testGetName() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, MalformedObjectNameException {		
		MBeanCollector testCollector = new MBeanCollectorImpl();
		String testName = "My TestData: %s";

		testCollector.setName(testName);
		assertEquals(testName, testCollector.getConstructedName(objectName));	

		List<String> nameSubstitution = new ArrayList<String>();
		nameSubstitution.add("a");
		testCollector.setNameParameters(nameSubstitution);
		assertEquals(String.format(testName, "test"), testCollector.getConstructedName(objectName));

		testCollector.setNameParameters(null);
		nameSubstitution.clear();
		nameSubstitution.add("X");
		testCollector.setNameAttributes(nameSubstitution);
		assertEquals(String.format(testName, "42"), testCollector.getConstructedName(objectName));
	}

	@Test
	public void testGetValues() throws InstanceNotFoundException, ReflectionException {	
		MBeanCollector testCollector = new MBeanCollectorImpl();
		
		Map<String, Object> values = testCollector.getValues(objectName);
		assertEquals(0, values.size());

		List<String> attributes = new ArrayList<String>();
		attributes.add("X");
		testCollector.setAttributes(attributes);
		values = testCollector.getValues(objectName);
		assertEquals(1, values.size());

		attributes.add("Y");
		testCollector.setAttributes(attributes);
		values = testCollector.getValues(objectName);
		assertEquals(2, values.size());
	}

}
