<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  JMole, https://bitbucket.org/awelen/jmole
  %%
  Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		
	<java class="java.beans.XMLDecoder">
		<object class="java.util.ArrayList">
			
<xsl:for-each select="measurements/measurement">
			<void method="add">
				<object class="net.welen.jmole.Configuration">
					<void property="Level"><int><xsl:value-of select="@level"/></int></void>
								
					<!-- MBeanFinder -->								
					<void property="mBeanFinder">
						<object class="net.welen.jmole.finder.MBeanFinderImpl">
							<void property="objectNameQueries">
								<object class="java.util.ArrayList">
	<xsl:for-each select="objectNameQueries/objectNameQuery">
									<void method="add">
										<string><xsl:value-of select="."/></string>
									</void>
	</xsl:for-each>								
								</object>							
							</void>											

		<xsl:if test="count(filters/attributeName)&gt;0">
							<void property="attributeName">
	              				<string><xsl:value-of select="filters/attributeName"/></string>
	            			</void>
		</xsl:if>						
		<xsl:if test="count(filters/attributeMatch)&gt;0">
							<void property="attributeMatch">
	              				<string><xsl:value-of select="filters/attributeMatch"/></string>
	            			</void>
		</xsl:if>												
		<xsl:if test="count(filters/parameterName)&gt;0">
							<void property="parameterName">
	              				<string><xsl:value-of select="filters/parameterName"/></string>
	            			</void>
		</xsl:if>						
		<xsl:if test="count(filters/parameterMatch)&gt;0">
							<void property="parameterMatch">
	              				<string><xsl:value-of select="filters/parameterMatch"/></string>
	            			</void>
		</xsl:if>												
		<xsl:if test="count(filters/className)&gt;0">
							<void property="className">
	              				<string><xsl:value-of select="filters/className"/></string>
	            			</void>
		</xsl:if>						
						</object>						
					</void>
					
					<!-- MBeanCollector -->					
					<void property="mBeanCollector">
						<object class="net.welen.jmole.collector.MBeanCollectorImpl">
							<void property="attributes">
								<object class="java.util.ArrayList">
	<xsl:for-each select="attributes/attribute">								
									<void method="add">
										<string><xsl:value-of select="@name"/></string>
									</void>
	</xsl:for-each>																	
								</object>
							</void>
	
	<xsl:if test="count(attributes/attribute/dataExtractor)&gt;0">													
							<void property="dataCollectorExtractors">
								<object class="java.util.HashMap">
		<xsl:for-each select="attributes/attribute">								
									<void method="put">
										<string><xsl:value-of select="@name"/></string>
										<object class="{dataExtractor/@class}">
											<void property="realAttribute">
												<string><xsl:value-of select="dataExtractor/@realName"/></string>
											</void>																		
											<void property="fakeAttribute">
												<string><xsl:value-of select="@name"/></string>
											</void>									
										</object>
									</void>
		</xsl:for-each>									
								</object>
							</void>
	</xsl:if>
									
							<void property="name">
								<string><xsl:value-of select="@name"/></string>
							</void>
			<xsl:if test="string-length(nameExtensions/parameters)&gt;0">
							<void property="nameParameters">
								<object class="java.util.ArrayList">
				<xsl:for-each select="nameExtensions/parameters/parameter">				
									<void method="add">
										<string><xsl:value-of select="."/></string>
									</void>
				</xsl:for-each>									
								</object>
							</void>
			</xsl:if>							
			<xsl:if test="string-length(nameExtensions/attributes)&gt;0">
							<void property="nameAttributes">
								<object class="java.util.ArrayList">
				<xsl:for-each select="nameExtensions/attributes/attribute">				
									<void method="add">
										<string><xsl:value-of select="."/></string>
									</void>
				</xsl:for-each>									
								</object>
							</void>
			</xsl:if>		
			<xsl:if test="string-length(nameExtensions/domainName)&gt;0">
							<void property="nameDomainNameParts">
								<object class="java.util.ArrayList">
				<xsl:for-each select="nameExtensions/domainName/partNum">				
									<void method="add">
										<int><xsl:value-of select="."/></int>
									</void>
				</xsl:for-each>									
								</object>
							</void>
			</xsl:if>		
			
								<!-- counterIntervals -->
							<xsl:if test="count(attributes/attribute/interval)&gt;0">					
											<void property="counterIntervals">
												<object class="java.util.HashMap">
								<xsl:for-each select="attributes/attribute">
									<xsl:if test="string-length(interval)&gt;0">
													<void method="put">
														<string><xsl:value-of select="@name"/></string>
														<int><xsl:value-of select="interval"/></int>
													</void>
									</xsl:if>
								</xsl:for-each>
												</object>						
											</void>
							</xsl:if>
														
						</object>
					</void>
														
					<!--  presentationInformation -->
					<void property="presentationInformation">
						<object class="net.welen.jmole.presentation.PresentationInformation">
							<void property="category">
								<string><xsl:value-of select="@category"/></string>
							</void>
							<void property="unit">
								<string><xsl:value-of select="unit"/></string>
							</void>				
				<xsl:if test="string-length(description)&gt;0">							
							<void property="description">
								<string><xsl:value-of select="description"/></string>
							</void>
				</xsl:if>							
	<xsl:if test="count(attributes/attribute/label)&gt;0">					
							<void property="attributeLabels">
								<object class="java.util.HashMap">
		<xsl:for-each select="attributes/attribute">
			<xsl:if test="string-length(label)&gt;0">
									<void method="put">
										<string><xsl:value-of select="@name"/></string>
										<string><xsl:value-of select="label"/></string>
									</void>
			</xsl:if>
		</xsl:for-each>																					
								</object>
							</void>
	</xsl:if>			
	
						<!-- attributeDescriptions -->
		<xsl:if test="count(attributes/attribute/description)&gt;0">					
						<void property="attributeDescriptions">
							<object class="java.util.HashMap">
			<xsl:for-each select="attributes/attribute">
				<xsl:if test="string-length(description)&gt;0">
								<void method="put">
									<string><xsl:value-of select="@name"/></string>
									<string><xsl:value-of select="description"/></string>
								</void>
				</xsl:if>
			</xsl:for-each>
							</object>
						</void>
		</xsl:if>												
		
						</object>
					</void>
										
					<!-- Thresholds -->
	<xsl:if test="count(attributes/attribute/thresholds)&gt;0">
					<void property="Thresholds">
						<object class="java.util.HashMap">							
		<xsl:for-each select="attributes/attribute">							
			<xsl:if test="count(thresholds/common)&gt;0">
							<void method="put">
								<string><xsl:value-of select="@name"/></string>							
								<object class="net.welen.jmole.threshold.Threshold">
				<xsl:if test="string-length(thresholds/common/warningLowThreshold)&gt;0">								
									<void property="WarningLowThreshold">
										<string><xsl:value-of select="thresholds/common/warningLowThreshold"/></string>
									</void>
				</xsl:if>									
				<xsl:if test="string-length(thresholds/common/warningHighThreshold)&gt;0">								
									<void property="WarningHighThreshold">
										<string><xsl:value-of select="thresholds/common/warningHighThreshold"/></string>
									</void>
				</xsl:if>									
				<xsl:if test="string-length(thresholds/common/criticalLowThreshold)&gt;0">								
									<void property="CriticalLowThreshold">
										<string><xsl:value-of select="thresholds/common/criticalLowThreshold"/></string>
									</void>
				</xsl:if>									
				<xsl:if test="string-length(thresholds/common/criticalHighThreshold)&gt;0">								
									<void property="CriticalHighThreshold">
										<string><xsl:value-of select="thresholds/common/criticalHighThreshold"/></string>
									</void>
				</xsl:if>
				<xsl:if test="string-length(thresholds/common/message)&gt;0">								
									<void property="message">
										<string><xsl:value-of select="thresholds/common/message"/></string>
									</void>
				</xsl:if>
				<xsl:if test="count(thresholds/individual)&gt;0">									
					<void property="IndividualThresholds">
						<object class="java.util.HashMap">
					<xsl:for-each select="thresholds/individual">
							<void method="put">
								<string><xsl:value-of select="@match"/></string>							
								<object class="net.welen.jmole.threshold.ThresholdValues">
							<xsl:if test="string-length(warningLowThreshold)&gt;0">
									<void property="WarningLowThreshold">
										<string><xsl:value-of select="warningLowThreshold"/></string>
									</void>									
							</xsl:if>	
							<xsl:if test="string-length(warningHighThreshold)&gt;0">							
									<void property="WarningHighThreshold">
										<string><xsl:value-of select="warningHighThreshold"/></string>
									</void>
							</xsl:if>								
							<xsl:if test="string-length(criticalLowThreshold)&gt;0">																				
									<void property="CriticalLowThreshold">
										<string><xsl:value-of select="criticalLowThreshold"/></string>
									</void>
							</xsl:if>												
							<xsl:if test="string-length(criticalHighThreshold)&gt;0">																
									<void property="CriticalHighThreshold">
										<string><xsl:value-of select="criticalHighThreshold"/></string>
									</void>
							</xsl:if>																												
							<xsl:if test="string-length(message)&gt;0">																
									<void property="message">
										<string><xsl:value-of select="message"/></string>
									</void>
							</xsl:if>																												
								</object>
							</void>							
					</xsl:for-each>						
						</object>
					</void>
				</xsl:if>
								</object>
						</void>				
			</xsl:if>
		</xsl:for-each>								
						</object>				
					</void>
	</xsl:if>
				</object>
			</void>
</xsl:for-each>
		</object>
	</java>
		
	</xsl:template>
</xsl:stylesheet>
