package net.welen.jmole.presentation;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.HashMap;

public class PresentationInformation {

	private String category = null;
	private String description = null;
	private String unit = null;
	private Map<String, String> attributeLabels = new HashMap<String, String>();
	private Map<String, String> attributeDescriptions = new HashMap<String, String>();

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getUnit() {
		return unit;
	}

	public void setAttributeLabels(Map<String, String> attributeLabels) {
		this.attributeLabels = attributeLabels;
	}

	public String translateAttributeLabel(String attributeName) {
		String label = attributeLabels.get(attributeName);
		if (label != null) {
			return label;
		}
		return attributeName;
	}

	public void setAttributeDescriptions(Map<String, String> attributeDescriptions) {
		this.attributeDescriptions = attributeDescriptions;
	}

	public String getAttributeDescription(String attributeName) {
		return attributeDescriptions.get(attributeName);
        }

}
