package net.welen.jmole.collector;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Map;

import javax.management.ObjectName;
import javax.management.MBeanException;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.ReflectionException;

/**
 * Describes the business interface of a MBean collector
 */
public interface MBeanCollector {
	
	/**
	 * Set the name of the measurement
	 * <p>
	 * Use %s to to indicate where provided attribute/paramter values should
	 * be injected when constructing the individual name for a measurement
	 * 
	 * @param name
	 */
	public void setName(String name);	
	public String getName();
	
	/**
	 * Configure if the name of the measurement should be completed by a dynamic
	 * content from the found MBeans attributes. 
	 * 
	 * @param nameAttributes A list of attributes to parse into the name of the measurement
	 */
	public void setNameAttributes(List<String> nameAttributes);
	
	/**
	 * Configure if the name of the measurement should be completed by a dynamic
	 * content from the found MBeans parameters. 
	 * 
	 * @param nameAttributes A list of parameters to parse into the name of the measurement
	 */
	public void setNameParameters(List<String> nameParameters);
	
	/**
	 * Which attributes from the found MBean should be collected
	 * 
	 * @param attributes A list of the attributes names
	 */
	public void setAttributes(List<String> attributes);
	public List<String> getAttributes();
	
	/**
	 * Specify of any of the collected attributes needs to be collected
	 * using a CollectorExtractor 
	 * 
	 * @param attributeExtractors A map of the attribute name and the CollectorExtractor to use 
	 */
	public void setDataCollectorExtractors(Map<String, DataCollectorExtractor> attributeExtractors);

	/**
	 * By using the supplied name (together with the NameParameters and NameAttribute lists)
	 * a complete name is constructed that may be dynamic in runtime
	 * 
	 * @param objectName The MBean of which attribute and parameters may be used
	 * 
	 * @return
	 * @throws MBeanException
	 * @throws AttributeNotFoundException
	 * @throws InstanceNotFoundException
	 * @throws ReflectionException
	 */
	public String getConstructedName(ObjectName objectName) throws MBeanException,
			AttributeNotFoundException, InstanceNotFoundException,
			ReflectionException;

	public Map<String, Object> getValues(ObjectName objectName) throws InstanceNotFoundException, ReflectionException;

}
