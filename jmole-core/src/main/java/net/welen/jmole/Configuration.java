package net.welen.jmole;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

import net.welen.jmole.collector.MBeanCollector;
import net.welen.jmole.finder.MBeanFinder;
import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.threshold.Threshold;

/**
 * Holds the configuration for all MBeans that should be collected
 */
public class Configuration {
	
	private MBeanFinder mbeanFinder = null;
	private MBeanCollector mbeanCollector = null;
	private PresentationInformation presentationInformation = null;
	private Map<String, Threshold> thresholds = new HashMap<String, Threshold>();
	private int level = 3;

	public MBeanFinder getMBeanFinder() {
		return mbeanFinder;
	}

	public void setMBeanFinder(MBeanFinder mbeanFinder) {
		this.mbeanFinder = mbeanFinder;
	}

	public MBeanCollector getMBeanCollector() {
		return mbeanCollector;
	}

	public void setMBeanCollector(MBeanCollector mbeanCollector) {
		this.mbeanCollector = mbeanCollector;
	}

	public PresentationInformation getPresentationInformation() {
		return presentationInformation;
	}

	public void setPresentationInformation(PresentationInformation presentationInformation) {
		this.presentationInformation = presentationInformation;
	}

	public void setThresholds(Map<String, Threshold> thresholds) {
		this.thresholds = thresholds;
	}
	
	public Map<String, Threshold> getThresholds() {
		return thresholds;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		if (level < 1 || level > 5) {
			throw new RuntimeException(JMole.CONFIG_LEVEL_PROPERTY + " must be 1-5");
		}
		this.level = level;
	}

}
