package net.welen.jmole;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.HashMap;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.MBeanServerNotification;
import javax.management.relation.MBeanServerNotificationFilter;
import javax.management.MBeanServerDelegate;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanException;
import javax.management.AttributeNotFoundException;
import javax.management.ReflectionException;
import javax.management.IntrospectionException;
import javax.xml.XMLConstants;
import javax.xml.transform.ErrorListener;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.google.gson.Gson;

import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.threshold.Threshold;

import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.beans.XMLDecoder;
import java.beans.ExceptionListener;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * The MBean implementation of JMole and more or less the main class
 */
public class JMole implements NotificationListener, JMoleMBean {

	private final static Logger LOG = Logger.getLogger(JMole.class.getName());

	static public final String OBJECT_NAME = "net.welen.jmole:service=jmole";
	static public final String CONFIG_FILENAMES_PROPERTY = "jmole.config.filenames";
	static public final String CONFIG_FILENAME_XSLT_PROPERTY_PREFIX = "jmole.config.xslt.";
	static public final String CONFIG_LEVEL_PROPERTY = "jmole.config.level";
	static public final String CONFIG_USE_OLD_CONFIGS_PROPERTY = "jmole.config.useOldConfigFormat";

	private MBeanServer server = Utils.getMBeanServer();
	private List<Configuration> configurations = new ArrayList<Configuration>();

	// Inner class that is used by XML libs
	private static class SimpleExceptionListener implements ExceptionListener {
		private Exception e = null;

		public void exceptionThrown(Exception e) {
			this.e = e;
		}

		Exception getException() {
			return e;
		}
	}

	/**
	 * Register the JMole MBean and a listener (that react to all new and
	 * removed MBeans)
	 * 
	 * @throws MalformedObjectNameException
	 * @throws InstanceAlreadyExistsException
	 * @throws MBeanRegistrationException
	 * @throws NotCompliantMBeanException
	 * @throws InstanceNotFoundException
	 */
	public void register() throws InstanceAlreadyExistsException, MBeanRegistrationException,
			NotCompliantMBeanException, MalformedObjectNameException, InstanceNotFoundException {
		LOG.log(Level.FINE, "Registering JMole MBean");
		server.registerMBean(this, new ObjectName(OBJECT_NAME));
		try {
			MBeanServerNotificationFilter filter = new MBeanServerNotificationFilter();
			filter.enableAllObjectNames();
			server.addNotificationListener(MBeanServerDelegate.DELEGATE_NAME, this, filter, null);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			server.unregisterMBean(new ObjectName(OBJECT_NAME));
			throw new RuntimeException(e);
		}
	}

	/**
	 * Unregister the JMole MBean and remove the listener
	 * 
	 * @throws MalformedObjectNameException
	 * @throws InstanceNotFoundException
	 * @throws MBeanRegistrationException
	 * @throws ListenerNotFoundException
	 */
	public void unregister() throws MalformedObjectNameException, InstanceNotFoundException, MBeanRegistrationException,
			ListenerNotFoundException {
		LOG.log(Level.FINE, "Removing JMole MBean");
		deactivateThresholds();
		try {
			server.removeNotificationListener(MBeanServerDelegate.DELEGATE_NAME, this);
		} finally {
			server.unregisterMBean(new ObjectName(OBJECT_NAME));
		}
	}

	@Override
	public synchronized void configure() throws MalformedObjectNameException, FileNotFoundException, MBeanException,
			AttributeNotFoundException, InstanceNotFoundException, ReflectionException, IntrospectionException {

		List<Configuration> newConfigurations = Collections.synchronizedList(new ArrayList<Configuration>());

		String fileNames = "JMole_JVM.xml";
		if (System.getProperty(CONFIG_FILENAMES_PROPERTY) != null) {
			fileNames = System.getProperty(CONFIG_FILENAMES_PROPERTY);
		}

		for (String fileName : fileNames.split("\\|")) {
			LOG.log(Level.INFO, "Configuring JMole with config file: " + fileName);

			XMLDecoder decoder = null;
			InputStream configStream = null;
			SimpleExceptionListener myListener = new SimpleExceptionListener();
			byte[] data = null;
			try {
				// Get inputstream
				if (new File(fileName).exists()) {
					configStream = new BufferedInputStream(new FileInputStream(fileName));
				} else {
					configStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
				}

				if (configStream == null) {
					LOG.log(Level.SEVERE, String.format("Unable to load config from '%s'", fileName));
					continue;
				}

				// Put all in memory
				try {
					data = readData(configStream);
				} catch (IOException e) {
					LOG.log(Level.SEVERE, String.format("Unable to load config from '%s'", fileName), e);
					continue;
				}

				// Replace all ${systemProperty} syntax
				data = replaceAllSystemProperties(data);

				// Validate and transform any new config files
				if (!Boolean.getBoolean(CONFIG_USE_OLD_CONFIGS_PROPERTY)) {

					String xslt = System.getProperty(CONFIG_FILENAME_XSLT_PROPERTY_PREFIX + fileName);
					if (xslt != null) {
						try {
							LOG.log(Level.INFO,
									String.format("Applying XSLT: '%s' on config file: '%s'", xslt, fileName));
							data = transformData(data, xslt);
						} catch (Exception e) {
							LOG.log(Level.SEVERE, String.format(
									"Unable to apply specified XSLT transformation to config file '%s'", fileName), e);
							continue;
						}
					}

					try {
						validateData(data);
					} catch (Exception e) {
						LOG.log(Level.SEVERE, String.format("Unable to validate config file '%s'", fileName), e);
						continue;
					}

					try {
						data = transformData(data, "META-INF/JMole.xsl");
					} catch (Exception e) {
						LOG.log(Level.SEVERE, String.format("Unable to transform config file '%s'", fileName), e);
						continue;
					}

				}

				// Parse data
				decoder = new XMLDecoder(new ByteArrayInputStream(data));
				decoder.setExceptionListener(myListener);

				Object newDecodedConfigurations = null;
				try {
					newDecodedConfigurations = decoder.readObject();
				} catch (ArrayIndexOutOfBoundsException e) {
					LOG.log(Level.SEVERE,
							String.format("Config file is corrupt '%s'\n== CONFIG START ==\n%s\n== CONFIG STOP ==",
									fileName, new String(data)), e);
					continue;
				}

				newConfigurations.addAll((List<Configuration>) newDecodedConfigurations);
			} finally {
				Exception exception = myListener.getException();
				if (exception != null) {
					LOG.log(Level.SEVERE,
							String.format("Config file is corrupt '%s'\n== CONFIG START ==\n%s\n== CONFIG STOP ==",
									fileName, new String(data)), exception);
				}

				if (decoder != null) {
					decoder.close();
				}
				if (configStream != null) {
					try {
						configStream.close();
					} catch (IOException e) {
						LOG.log(Level.WARNING, "Couldn't close configStream", e);
					}
				}
			}
		}

		// Remove all configurations that don't meet the level criteria
		int level = Integer.getInteger(CONFIG_LEVEL_PROPERTY, 3);
		if (level < 1 || level > 5) {
			throw new RuntimeException(CONFIG_LEVEL_PROPERTY + " must be 1-5");
		}
		List<Configuration> levelFilteredConfigurations = new ArrayList<Configuration>();
		for (Configuration configuration : newConfigurations) {
			if (configuration.getLevel() <= level) {
				levelFilteredConfigurations.add(configuration);
			}
		}

		deactivateThresholds();
		configurations = levelFilteredConfigurations;
		discover();
		activateThresholds();
	}

	private byte[] replaceAllSystemProperties(byte[] data) {
		String stringData = new String(data);

		for (Entry<Object, Object> propEntry : System.getProperties().entrySet()) {
			String key = (String) propEntry.getKey();
			String value = (String) propEntry.getValue();
			stringData = stringData.replaceAll("\\$\\{" + key + ":.*\\}", value);
			stringData = stringData.replaceAll("\\$\\{" + key + "\\}", value);
		}

		stringData = stringData.replaceAll("\\$\\{.*:|\\}", "");

		return stringData.getBytes();
	}

	private byte[] readData(InputStream inputStream) throws IOException {
		ByteArrayOutputStream data = new ByteArrayOutputStream();

		try {
			byte[] buffer = new byte[1024];
			int read = 0;
			while ((read = inputStream.read(buffer)) != -1) {
				data.write(buffer, 0, read);
			}
		} finally {
			if (data != null) {
				try {
					data.close();
				} catch (IOException e) {
					// Do nothing
				}
			}
		}
		return data.toByteArray();
	}

	private void validateData(byte[] data) throws SAXException, IOException {
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		try {
			schema = factory.newSchema(new StreamSource(
					Thread.currentThread().getContextClassLoader().getResourceAsStream("META-INF/JMole.xsd")));
		} catch (SAXParseException e) {
			ClassLoader tccl = Thread.currentThread().getContextClassLoader();
			try {
				Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
				schema = factory.newSchema(new StreamSource(
						Thread.currentThread().getContextClassLoader().getResourceAsStream("META-INF/JMole.xsd")));
			} finally {
				Thread.currentThread().setContextClassLoader(tccl);
			}
		}
		Validator validator = schema.newValidator();
		validator.validate(new StreamSource(new ByteArrayInputStream(data)));
	}

	private byte[] transformData(byte[] data, String xsltFile) throws TransformerException {
		ByteArrayOutputStream answer = new ByteArrayOutputStream();
		StreamResult outputStream = new StreamResult(answer);

		TransformerFactory factory = TransformerFactory.newInstance();
		factory.setErrorListener(new ErrorListener() {		
			@Override
			public void warning(TransformerException exception) throws TransformerException {
				LOG.log(Level.FINE, exception.getMessage(), exception);
			}			
			@Override
			public void fatalError(TransformerException exception) throws TransformerException {
				LOG.log(Level.FINE, exception.getMessage(), exception);
			}			
			@Override
			public void error(TransformerException exception) throws TransformerException {
				LOG.log(Level.FINE, exception.getMessage(), exception);
			}
		});		
		try {
			LOG.log(Level.FINE, "Getting the XSLT file from Thread.currentThread().getContextClassLoader()");
			Source xslt = new StreamSource(Thread.currentThread().getContextClassLoader().getResourceAsStream(xsltFile));
			Transformer transformer = factory.newTransformer(xslt);
			transformer.transform(new StreamSource(new ByteArrayInputStream(data)), outputStream);
		} catch (Exception e) {
			ClassLoader tccl = Thread.currentThread().getContextClassLoader();
			try {
				LOG.log(Level.FINE, "Getting the XSLT file from getClass().getClassLoader()");
				Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
				Source xslt = new StreamSource(Thread.currentThread().getContextClassLoader().getResourceAsStream(xsltFile));
				Transformer transformer = factory.newTransformer(xslt);
				transformer.transform(new StreamSource(new ByteArrayInputStream(data)), outputStream);
			} finally {
				Thread.currentThread().setContextClassLoader(tccl);
			}
		}
			
		return answer.toByteArray();
	}

	@Override
	public synchronized void discover() throws MBeanException, AttributeNotFoundException, InstanceNotFoundException,
			ReflectionException, IntrospectionException {
		LOG.log(Level.FINE, "Running JMole discovery");
		for (Configuration configuration : configurations) {
			configuration.getMBeanFinder().updateMatchingObjectNames();
		}
	}

	/**
	 * Get the current configuration
	 * 
	 * @return The configuration in for av classes
	 */
	public List<Configuration> getConfiguration() {
		return configurations;
	}

	@Override
	public Map<String, List<Map<String, Map<String, Object>>>> collectMeasurements()
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException {
		return collectMeasurements((Map<Object, PresentationInformation>) null);
	}

	public Map<String, List<Map<String, Map<String, Object>>>> collectMeasurements(Map<Object, PresentationInformation> presentationInformation)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException {

		Map<String, List<Map<String, Map<String, Object>>>> answer = new HashMap<String, List<Map<String, Map<String, Object>>>>();
		for (Configuration configuration : configurations) {
			String category = configuration.getPresentationInformation().getCategory();

			if (answer.containsKey(category)) {
				continue;
			}

			Map<String, Map<String, Object>> data = collectMeasurements(category, presentationInformation);

			if (!data.isEmpty()) {
				if (answer.get(category) == null) {
					answer.put(category, new ArrayList<Map<String, Map<String, Object>>>());
				}
				answer.get(category).add(data);
			}

		}
		return answer;
	}

	@Override
	public String collectMeasurementsAsJSON()
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException {
		return new Gson().toJson(collectMeasurements());
	}

	@Override
	public Map<String, Map<String, Object>> collectMeasurements(String category)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException {		
		return collectMeasurements(category, (Map<Object, PresentationInformation>) null);
	}

	public Map<String, Map<String, Object>> collectMeasurements(String category, Map<Object, PresentationInformation> presentationInformation)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException {

		Map<String, Map<String, Object>> answer = new HashMap<String, Map<String, Object>>();
		for (Configuration configuration : configurations) {
			if (configuration.getPresentationInformation().getCategory().equals(category)) {
				for (ObjectName objectName : configuration.getMBeanFinder().getMatchingObjectNames()) {
					if (presentationInformation != null) {
						LOG.log(Level.FINE, "PresentationInformation saved for: " + category + configuration.getMBeanCollector().getConstructedName(objectName));
						presentationInformation.put(category + configuration.getMBeanCollector().getConstructedName(objectName), configuration.getPresentationInformation());
					}
					answer.put(configuration.getMBeanCollector().getConstructedName(objectName),
							configuration.getMBeanCollector().getValues(objectName));
				}
			}
		}
		return answer;
	}

	@Override
	public String collectMeasurementsAsJSON(String category)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException {
		return new Gson().toJson(collectMeasurements(category));
	}

	@Override
	public Map<String, Object> collectMeasurements(String category, String name)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException {

		for (Configuration configuration : configurations) {
			if (configuration.getPresentationInformation().getCategory().equals(category)) {
				for (ObjectName objectName : configuration.getMBeanFinder().getMatchingObjectNames()) {
					if (configuration.getMBeanCollector().getConstructedName(objectName).equals(name)) {
						return configuration.getMBeanCollector().getValues(objectName);
					}
				}
			}
		}
		return null;
	}

	@Override
	public String collectMeasurementsAsJSON(String category, String name)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException {
		return new Gson().toJson(collectMeasurements(category, name));
	}

	@Override
	public Object collectMeasurement(String category, String name, String attribute)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException {

		return collectMeasurements(category, name).get(attribute);
	}

	public String collectMeasurementAsJSON(String category, String name, String attribute)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException {
		return new Gson().toJson(collectMeasurement(category, name, attribute));
	}

	/**
	 * Call back method used by the MBean Server notification mechanism. May
	 * trigger a re-configuration if new MBeans are registred (or unregistred(
	 */
	public void handleNotification(Notification notification, Object handback) {
		LOG.log(Level.FINE, "MBean notification recieved", notification);
		if (notification instanceof MBeanServerNotification) {
			if (notification.getType().equals(MBeanServerNotification.REGISTRATION_NOTIFICATION)
					|| notification.getType().equals(MBeanServerNotification.UNREGISTRATION_NOTIFICATION)) {
				LOG.log(Level.FINE, "MBean notication recieved. Reconfiguring");
				try {
					discover();
				} catch (Exception e) {
					LOG.log(Level.SEVERE, "Reconfiguring of JMole failed.", e);
				}
			}
		}
	}

	private void deactivateThresholds() {
		LOG.log(Level.FINE, "Stopping threshold threads");

		for (Configuration configuration : configurations) {
			for (Threshold threshold : configuration.getThresholds().values()) {
				threshold.stopThread();
			}
		}
	}

	private void activateThresholds() {
		for (Configuration configuration : configurations) {
			for (Entry<String, Threshold> entry : configuration.getThresholds().entrySet()) {
				String key = entry.getKey();
				Threshold threshold = entry.getValue();

				StringBuffer threadName = new StringBuffer(
						"JMole Threshold Thread: " + configuration.getPresentationInformation().getCategory() + "->"
								+ configuration.getMBeanCollector().getName() + "->" + key);
				if (!threshold.getWarningLowThreshold().isEmpty()) {
					threadName.append("  WarningLow");
				}
				if (!threshold.getWarningHighThreshold().isEmpty()) {
					threadName.append("  WarningHigh");
				}
				if (!threshold.getCriticalLowThreshold().isEmpty()) {
					threadName.append("  CriticalLow");
				}
				if (!threshold.getCriticalHighThreshold().isEmpty()) {
					threadName.append("  CriticalHigh");
				}

				threshold.setMBeanFinder(configuration.getMBeanFinder());
				threshold.setMBeanCollector(configuration.getMBeanCollector());
				threshold.setAttribute(key);

				LOG.log(Level.FINE, "Starting thread: " + threadName);
				new Thread(threshold, threadName.toString()).start();
			}
		}
	}

	@Override
	public Map<String, Map<String, String>> warningMessages()
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		return constructMessageData(true);
	}

	@Override
	public String warningMessagesAsJSON()
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		return new Gson().toJson(constructMessageData(true));
	}

	@Override
	public Map<String, Map<String, String>> criticalMessages()
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		return constructMessageData(false);
	}

	@Override
	public String criticalMessagesAsJSON()
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		return new Gson().toJson(constructMessageData(false));
	}

	private Map<String, Map<String, String>> constructMessageData(boolean warning)
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		Map<String, Map<String, String>> answer = new HashMap<String, Map<String, String>>();
		for (Configuration configuration : configurations) {
			for (Threshold threshold : configuration.getThresholds().values()) {
				Map<String, String> thresholdMap = new HashMap<String, String>();
				Set<Entry<ObjectName, String>> entrySet = null;
				if (warning) {
					entrySet = threshold.getWarningMessages().entrySet();
				} else {
					entrySet = threshold.getCriticalMessages().entrySet();
				}
				for (Entry<ObjectName, String> entry : entrySet) {
					ObjectName objectName = entry.getKey();
					String msg = entry.getValue();
					thresholdMap.put(configuration.getMBeanCollector().getConstructedName(objectName), msg);
				}
				if (!thresholdMap.isEmpty()) {
					answer.put(configuration.getPresentationInformation().getCategory(), thresholdMap);
				}
			}
		}
		return answer;
	}

}
