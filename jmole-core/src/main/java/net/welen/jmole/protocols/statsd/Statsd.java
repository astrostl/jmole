package net.welen.jmole.protocols.statsd;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.ReflectionException;

import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;

import net.welen.jmole.JMole;

public class Statsd implements StatsdMBean, Runnable {

	private final static Logger LOG = Logger.getLogger(Statsd.class.getName());

	private static final char SEP = '.';
	
	private static String PROPERTY_STATSD_ENABLED = "jmole.protocol.statsd.enabled";
	private static String PROPERTY_STATSD_PREFIX = "jmole.protocol.statsd.prefix";
	private static String PROPERTY_STATSD_HOST = "jmole.protocol.statsd.host";
	private static String PROPERTY_STATSD_PORT = "jmole.protocol.statsd.port";
	private static String PROPERTY_STATSD_INTERVAL = "jmole.protocol.statsd.interval";

	private StatsDClient statsd = null;
	private String prefix;
	private String host;
	private Integer port;
	private Long interval;
	
	private boolean stopped = false;
	private boolean statsdStopped = false;
	private Thread collector;
	private JMole jmole;
	
	@Override
	public boolean isEnabled() {
		return Boolean.getBoolean(PROPERTY_STATSD_ENABLED);
	}
	
	@Override
	public void startProtocol(JMole jmole) throws Exception {
		this.jmole = jmole;
		
		prefix = System.getProperty(PROPERTY_STATSD_PREFIX);
		if (prefix == null) {
			prefix = "JMole";
		}

		host = System.getProperty(PROPERTY_STATSD_HOST);
		if (host == null) {
			host = "localhost";
		}

		port = Integer.getInteger(PROPERTY_STATSD_PORT);
		if (port == null) {
			port = 8125;
		}

		interval = Long.getLong(PROPERTY_STATSD_INTERVAL);
		if (interval == null) {
			interval = 60000L;
		}

		statsd = new NonBlockingStatsDClient(prefix, host, port);
		collector = new Thread(this);
		collector.setName("JMole statsd collector thread");
		collector.start();
		LOG.log(Level.INFO, "JMole StatsD protocol started: " + prefix + " " + host + " " + port);
	}
	

	@Override
	public void stopProtocol() throws Exception {
		LOG.log(Level.INFO, "Stopping JMole StatsD protocol");
		stopped = true;
		collector.interrupt();
		while (!statsdStopped) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				LOG.log(Level.FINE, e.getMessage(), e);
			}
		}
		LOG.log(Level.INFO, "JMole StatsD protocol stopped");
	}
	
	@Override
	public String getPrefix() {
		return prefix;
	}

	@Override
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@Override
	public String getHost() {
		return host;
	}

	@Override
	public void setHost(String host) {
		this.host = host;
	}

	@Override
	public int getPort() {
		return port;
	}

	@Override
	public void setPort(int port) {
		this.port = port;
	}

	@Override
	public long getInterval() {
		return interval;
	}

	@Override
	public void setInterval(long interval) {
		this.interval = interval;
	}

	@Override
	public void run() {
		stopped  = false;
		statsdStopped = false;
		try {
			while (!stopped) {				
				try {
					Thread.sleep(interval);
				} catch (InterruptedException e) {
					LOG.log(Level.FINE, e.getMessage(), e);
				}

				try {
					collectMeasurements();
					sendWarnings();
					sendCriticals();
				} catch (Exception e) {
					LOG.log(Level.SEVERE, e.getMessage(), e);
				}
				
			}
		} finally {		
			statsdStopped = true;
		}
	}

	private void collectMeasurements() throws InstanceNotFoundException, AttributeNotFoundException, ReflectionException, MBeanException {
		for (Entry<String, List<Map<String, Map<String, Object>>>> categoryEntry : jmole.collectMeasurements().entrySet()) {
			Iterator<Map<String, Map<String, Object>>> iter = categoryEntry.getValue().iterator();
			while (iter.hasNext()) {
				for (Entry<String, Map<String, Object>> nameEntry : iter.next().entrySet()) {
					for (Entry<String, Object> attributeEntry : nameEntry.getValue().entrySet()) {

						String key = String.format("%s%c%s%c%s", 
														fixKeyName(categoryEntry.getKey()),
														SEP, fixKeyName(nameEntry.getKey()),
														SEP, fixKeyName(attributeEntry.getKey()));
						try {
							if (attributeEntry.getValue() != null) {
								Double value = Double.parseDouble(attributeEntry.getValue().toString());
								LOG.log(Level.FINE, key + " = " + value);														
								statsd.recordGaugeValue(key, value);
							}
						} catch (NumberFormatException e) {
							LOG.log(Level.SEVERE, e.getMessage() + ": " + key, e);
						}						
					}
				}
			}
		}
	}
	
	private String fixKeyName(String key) {
		return key.replaceAll("[^a-zA-Z0-9]", "_");
	}

	private void sendWarnings() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		for ( Entry<String, Map<String, String>> categoryEntry : jmole.warningMessages().entrySet()) {
			for (Entry<String, String> entry : categoryEntry.getValue().entrySet()) {
				statsd.recordSetEvent(entry.getKey(), entry.getValue());
			}
		}
	}

	private void sendCriticals() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		for ( Entry<String, Map<String, String>> categoryEntry : jmole.criticalMessages().entrySet()) {
			for (Entry<String, String> entry : categoryEntry.getValue().entrySet()) {
				statsd.recordSetEvent(entry.getKey(), entry.getValue());
			}
		}
	}

}
