package net.welen.jmole.protocols.munin;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import net.welen.jmole.protocols.Protocol;

public interface MuninMBean extends Protocol {
	
	public String getName();

	public void setName(String name);

	public String getAddress();

	public void setAddress(String address);

	public Integer getPort();

	public void setPort(Integer port);

	public Integer getTcpReadTimeOut();

	public void setTcpReadTimeOut(Integer timeout);

	public Integer getMaxThreads();

	public void setMaxThreads(Integer maxThreads);

	public Integer getCurrentThreads();
	
}
