package net.welen.jmole.protocols.nrpe;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import it.jnrpe.JNRPE;
import it.jnrpe.JNRPEBuilder;
import it.jnrpe.commands.CommandDefinition;
import it.jnrpe.commands.CommandOption;
import it.jnrpe.commands.CommandRepository;
import it.jnrpe.plugins.IPluginInterface;
import it.jnrpe.plugins.PluginDefinition;
import it.jnrpe.plugins.PluginOption;
import it.jnrpe.plugins.PluginRepository;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.welen.jmole.JMole;

public class NRPE implements NRPEMBean {

	private final static Logger LOG = Logger.getLogger(NRPE.class.getName());
	
	private static String PROPERTY_NRPE_ENABLED = "jmole.protocol.nrpe.enabled";
	private static String PROPERTY_NRPE_ADDRESS = "jmole.protocol.nrpe.address";
	private static String PROPERTY_NRPE_PORT = "jmole.protocol.nrpe.port";
	private static String PROPERTY_NRPE_ACCEPTED_HOSTS = "jmole.protocol.nrpe.acceptedHosts";
	private static String PROPERTY_NRPE_USE_SSL = "jmole.protocol.nrpe.useSSL";
	
	private String address;
	private Integer port;
	private String acceptedHosts[] = {"127.0.0.1"};
	private Boolean useSSL;
	private JNRPE engine = null;

	@Override
	public void startProtocol(JMole jmole) throws Exception {

		address = System.getProperty(PROPERTY_NRPE_ADDRESS);
		if (address == null) {
			address = "127.0.0.1";
		}

		port = Integer.getInteger(PROPERTY_NRPE_PORT);
		if (port == null) {
			port = 5666;
		}

		String acceptedHostsString = System.getProperty(PROPERTY_NRPE_ACCEPTED_HOSTS);
		if (acceptedHostsString != null) {
			acceptedHosts = acceptedHostsString.split(",");
		}
		
		useSSL = Boolean.getBoolean(PROPERTY_NRPE_USE_SSL);
		if (useSSL == null) {
			useSSL = false;
		}

		LOG.log(Level.INFO, "JMole NRPE protocol starting");
		LOG.log(Level.FINE,  address + ", " + port + ", " + Arrays.toString(acceptedHosts) + ", " + useSSL);
		
		// Create the plugins
		IPluginInterface warningsStatusPlugin = new StatusJNRPEPlugin(jmole, true);		
		PluginDefinition warningsStatusPluginDef = new PluginDefinition("WarningsStatusPlugin", "JMole Warnings Status Plugin", warningsStatusPlugin);

		IPluginInterface criticalsStatusPlugin = new StatusJNRPEPlugin(jmole, false);		
		PluginDefinition criticalsStatusPluginDef = new PluginDefinition("CriticalsStatusPlugin", "JMole Critical Warnings Status Plugin", criticalsStatusPlugin);

		IPluginInterface getValuePlugin = new GetValueJNRPEPlugin(jmole);
		PluginDefinition getValuePluginDef = new PluginDefinition("GetValuePlugin", "JMole GetValue Plugin", getValuePlugin);
		getValuePluginDef.addOption(new PluginOption()
									.setOption("c")
									.setArgName("category")
									.setHasArgs(true)
									.setArgsOptional(false)
									.setRequired(true));		
		getValuePluginDef.addOption(new PluginOption()
									.setOption("n")
									.setArgName("name")
									.setHasArgs(true)
									.setArgsOptional(false)
									.setRequired(true));		
		getValuePluginDef.addOption(new PluginOption()
									.setOption("a")
									.setArgName("attribute")
									.setHasArgs(true)
									.setArgsOptional(false)
									.setRequired(true));
		getValuePluginDef.addOption(new PluginOption()
									.setOption("w")
									.setArgName("warning")
									.setHasArgs(true)
									.setArgsOptional(false)
									.setRequired(false));
		getValuePluginDef.addOption(new PluginOption()
									.setOption("e")
									.setArgName("critical")
									.setHasArgs(true)
									.setArgsOptional(false)
									.setRequired(false));

		// Create the plugin repository
		PluginRepository pluginRepository = new PluginRepository();
		pluginRepository.addPluginDefinition(warningsStatusPluginDef);
		pluginRepository.addPluginDefinition(criticalsStatusPluginDef);
		pluginRepository.addPluginDefinition(getValuePluginDef);
		
		// Create the commands
		CommandDefinition warningStatusCommand = new CommandDefinition("checkWarnings", "WarningsStatusPlugin");
		CommandDefinition criticalStatusCommand = new CommandDefinition("checkCriticals", "CriticalsStatusPlugin");
		CommandDefinition specificCommand = new CommandDefinition("getValue", "GetValuePlugin")
				.addArgument(new CommandOption("c", "$ARG1$"))
				.addArgument(new CommandOption("n", "$ARG2$"))
				.addArgument(new CommandOption("a", "$ARG3$"))
				.addArgument(new CommandOption("w", "$ARG4$"))
				.addArgument(new CommandOption("e", "$ARG5$"));
		
		// Create the command repository
		CommandRepository commandRepository = new CommandRepository();
		commandRepository.addCommandDefinition(warningStatusCommand);
		commandRepository.addCommandDefinition(criticalStatusCommand);
		commandRepository.addCommandDefinition(specificCommand);
		
		// Start JNRPE
		JNRPEBuilder jnrpeBuilder = JNRPEBuilder.forRepositories(pluginRepository, commandRepository);
		jnrpeBuilder.acceptParams(true);
		
		for (String host : getAcceptedHosts()) {			
			jnrpeBuilder.acceptHost(host);
		} 
		engine = jnrpeBuilder.build();
	
		// TODO How to handle SSL keys?
		engine.listen(address, port, useSSL);
		
		LOG.log(Level.INFO, "JMole NRPE started.");
	}

	@Override
	public void stopProtocol() throws Exception {
		LOG.info("Stopping JMole NRPE");
		if (engine != null) {
			engine.shutdown();
		}
		LOG.info("JMole NRPE stopped");		
	}

	@Override
	public boolean isEnabled() {
		return Boolean.getBoolean(PROPERTY_NRPE_ENABLED);
	}

	@Override
	public String getAddress() {
		return address;
	}

	@Override
	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public Integer getPort() {
		return port;
	}

	@Override
	public void setPort(Integer port) {
		this.port = port; 
	}

	@Override
	public String[] getAcceptedHosts() {
		return acceptedHosts.clone();
	}

	@Override
	public void setAcceptedHosts(String[] acceptedHosts) {
		this.acceptedHosts = acceptedHosts.clone();
	}

	@Override
	public Boolean isUseSSL() {
		return useSSL;
	}

	@Override
	public void setUseSSL(Boolean useSSL) {
		this.useSSL = useSSL;
	}

}
