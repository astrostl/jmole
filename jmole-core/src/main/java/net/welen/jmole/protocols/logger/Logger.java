package net.welen.jmole.protocols.logger;

import java.math.BigDecimal;
import java.util.HashMap;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.ReflectionException;

import net.welen.jmole.JMole;
import net.welen.jmole.presentation.PresentationInformation;

public class Logger implements LoggerMBean, Runnable {

	private final static java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(Logger.class.getName());

	private static final char SEP = '/';
	
	private static String PROPERTY_LOGGER_ENABLED = "jmole.protocol.logger.enabled";
	private static String PROPERTY_LOGGER_NAME = "jmole.protocol.logger.name";
	private static String PROPERTY_LOGGER_LEVEL = "jmole.protocol.logger.level";
	private static String PROPERTY_LOGGER_INTERVAL = "jmole.protocol.logger.interval";	

	private Level level;
	private Long interval;
	private java.util.logging.Logger protocolLogger = java.util.logging.Logger.getLogger("JMole");
	
	private boolean stopped = false;
	private boolean loggerStopped = false;
	private Thread collector;
	private JMole jmole;
	
	@Override
	public boolean isEnabled() {
		return Boolean.getBoolean(PROPERTY_LOGGER_ENABLED);
	}
	
	@Override
	public void startProtocol(JMole jmole) throws Exception {
		this.jmole = jmole;
		
		String levelString = System.getProperty(PROPERTY_LOGGER_LEVEL);
		if (levelString == null) {
			levelString = "INFO";
		}
		level = Level.parse(levelString);

		String name = System.getProperty(PROPERTY_LOGGER_NAME);
		if (name != null) {
			protocolLogger = java.util.logging.Logger.getLogger(name);
		}		
		
		interval = Long.getLong(PROPERTY_LOGGER_INTERVAL);
		if (interval == null) {
			interval = 60000L;
		}

		collector = new Thread(this);
		collector.setName("JMole Logger protocol thread");
		collector.start();
		LOG.log(Level.INFO, "JMole Logger protocol started: Level=" + levelString + ", Interval=" + interval);
	}
	

	@Override
	public void stopProtocol() throws Exception {
		LOG.log(Level.INFO, "Stopping JMole Logger protocol");
		stopped = true;
		collector.interrupt();
		while (!loggerStopped) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				LOG.log(Level.FINE, e.getMessage(), e);
			}
		}
		LOG.log(Level.INFO, "JMole Logger protocol stopped");
	}

	@Override
	public String getLevel() {
		return level.getName();
	}

	@Override
	public void setLevel(String level) {
		this.level = Level.parse(level);
	}

	@Override
	public long getInterval() {
		return interval;
	}

	@Override
	public void setInterval(long interval) {
		this.interval = interval;
	}
	
	@Override
	public void run() {
		stopped  = false;
		loggerStopped = false;
		try {
			while (!stopped) {				
				try {
					Thread.sleep(interval);
				} catch (InterruptedException e) {
					LOG.log(Level.FINE, e.getMessage(), e);
				}

				try {
					logMeasurements();
					logWarnings();
					logCriticals();
				} catch (Exception e) {
					LOG.log(Level.SEVERE, e.getMessage(), e);
				}
				
			}
		} finally {		
			loggerStopped = true;
		}
	}

	private void logMeasurements() throws InstanceNotFoundException, AttributeNotFoundException, ReflectionException, MBeanException {
		Map<Object, PresentationInformation> presentationInformationMap = new HashMap<Object, PresentationInformation>();
		for (Entry<String, List<Map<String, Map<String, Object>>>> categoryEntry : jmole.collectMeasurements(presentationInformationMap).entrySet()) {
			Iterator<Map<String, Map<String, Object>>> iter = categoryEntry.getValue().iterator();
			while (iter.hasNext()) {
				for (Entry<String, Map<String, Object>> nameEntry : iter.next().entrySet()) {
					for (Entry<String, Object> attributeEntry : nameEntry.getValue().entrySet()) {
						
						String piKey = categoryEntry.getKey() + nameEntry.getKey(); 
						PresentationInformation presentationInformation = presentationInformationMap.get(piKey);
						if (presentationInformation == null) {
							LOG.severe("No presentation information found for: " + piKey + ", Skipping it");
							continue;
						}
						
						String key = String.format("%s%c%s%c%s", 
														categoryEntry.getKey(),
														SEP, nameEntry.getKey(),
														SEP, attributeEntry.getKey());
						try {
							if (attributeEntry.getValue() != null) {
								String value = new BigDecimal(Double.parseDouble(attributeEntry.getValue().toString())).toPlainString();
								String attributeDescription = presentationInformation.getAttributeDescription(attributeEntry.getKey());
								String logString = String.format("[%s] %s (%s) = %s %s"
										, key
										, presentationInformation.translateAttributeLabel(attributeEntry.getKey())
										, attributeDescription != null ? attributeDescription : ""
										, value
										, presentationInformation.getUnit()
										);
								protocolLogger.log(level, logString);
							}
						} catch (NumberFormatException e) {
							LOG.log(Level.SEVERE, e.getMessage() + ": " + key, e);
						}						
					}
				}
			}
		}
	}
	
	private void logWarnings() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		for ( Entry<String, Map<String, String>> categoryEntry : jmole.warningMessages().entrySet()) {
			for (Entry<String, String> entry : categoryEntry.getValue().entrySet()) {
				protocolLogger.log(Level.WARNING, entry.getKey() + ": " + entry.getValue());				
			}
		}
	}

	private void logCriticals() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		for ( Entry<String, Map<String, String>> categoryEntry : jmole.criticalMessages().entrySet()) {
			for (Entry<String, String> entry : categoryEntry.getValue().entrySet()) {				
				protocolLogger.log(Level.SEVERE, entry.getKey() + ": " + entry.getValue());
			}
		}
	}

}
