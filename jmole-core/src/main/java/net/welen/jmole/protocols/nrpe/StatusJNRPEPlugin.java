package net.welen.jmole.protocols.nrpe;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.welen.jmole.JMole;
import it.jnrpe.ICommandLine;
import it.jnrpe.ReturnValue;
import it.jnrpe.Status;
import it.jnrpe.plugins.IPluginInterface;

public class StatusJNRPEPlugin implements IPluginInterface {

	private final static Logger LOG = Logger.getLogger(NRPE.class.getName());
	
	private JMole jmole;
	
	private boolean warnings = true;
	
	public StatusJNRPEPlugin(JMole jmole, boolean warnings) {
		this.jmole = jmole;
		this.warnings = warnings;
	}
	
	public ReturnValue execute(ICommandLine commandLine) {
									
		try {
			
			if (!warnings) {
				// Critical?
				Map<String, Map<String, String>> critical = jmole.criticalMessages();
	
				if (critical.size() > 0) {
					LOG.log(Level.FINE, "Returning: CRITICAL " + critical);
					return new ReturnValue(Status.CRITICAL, critical.toString());
				}
			} else {	
				// Warnings
				Map<String, Map<String, String>> warnings = jmole.warningMessages();
	
				if (warnings.size() > 0) {
					LOG.log(Level.FINE, "Returning: WARNINGS " + warnings);
					return new ReturnValue(Status.WARNING, warnings.toString());
				}
			}
			
			LOG.log(Level.FINE, "Returning: OK ");			
			return new ReturnValue(Status.OK, "OK - " + "No problems detected.");
			
		} catch (Throwable t) {
			LOG.log(Level.SEVERE, t.getMessage(), t);
			return new ReturnValue(Status.UNKNOWN, "");
		}
	}
		
}
