package net.welen.jmole.protocols.munin;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.welen.jmole.JMole;

public class Munin implements MuninMBean, Runnable {

	private final static Logger LOG = Logger.getLogger(Munin.class.getName());
	
	private static String PROPERTY_MUNIN_ENABLED = "jmole.protocol.munin.enabled";
	private static String PROPERTY_MUNIN_NAME = "jmole.protocol.munin.name";
	private static String PROPERTY_MUNIN_ADDRESS = "jmole.protocol.munin.address";
	private static String PROPERTY_MUNIN_PORT = "jmole.protocol.munin.port";
	private static String PROPERTY_MUNIN_TCP_READ_TIMOUT = "jmole.protocol.munin.tcpReadTimeOut";
	private static String PROPERTY_MUNIN_MAX_THREADS = "jmole.protocol.munin.maxThreads";
	
	private ServerSocket serverSocket;
	private String name;
	private String address;
	private Integer port;
	private Integer tcpReadTimeOut;
	private Integer maxThreads = 5;
	private boolean stopped = false;
	private boolean socketStopped = true;
	protected int currentThreads;
	protected JMole jmole;
	
	public boolean isEnabled() {
		return Boolean.getBoolean(PROPERTY_MUNIN_ENABLED);
	}
	
	@Override
	public void startProtocol(JMole jmole) throws Exception {
		this.jmole = jmole;
		
		name = System.getProperty(PROPERTY_MUNIN_NAME);
		if (name == null) {
			name = "JMole";
		}
		
		address = System.getProperty(PROPERTY_MUNIN_ADDRESS);
		if (address == null) {
			address = "localhost";
		}

		port = Integer.getInteger(PROPERTY_MUNIN_PORT);
		if (port == null) {
			port = 4949;
		}

		tcpReadTimeOut = Integer.getInteger(PROPERTY_MUNIN_TCP_READ_TIMOUT);
		if (tcpReadTimeOut == null) {
			tcpReadTimeOut = 10000;
		}

		maxThreads = Integer.getInteger(PROPERTY_MUNIN_MAX_THREADS);
		if (maxThreads == null) {
			maxThreads = 5;
		}

		LOG.log(Level.INFO, "JMole Munin protocol starting; " + address + ":" + port);
		LOG.log(Level.FINE,  tcpReadTimeOut + ", " + maxThreads);
		serverSocket = new ServerSocket(port, -1, InetAddress.getByName(address));
		serverSocket.setSoTimeout(1000);
		
		new Thread(this).start();
	}
	

	@Override
	public void stopProtocol() throws Exception {
		stopped = true;
		while (!socketStopped) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				LOG.log(Level.SEVERE, e.getMessage(), e);
			}
		}
		LOG.log(Level.INFO, "JMole Munin protocol stopped");
	}
	
	public void run() {
		stopped = false;
		socketStopped = false;
		try {
			while (!stopped) {
				try {
					Socket socket = serverSocket.accept();					
					new MuninSocketHandler(socket, this).start();
				} catch (SocketTimeoutException e) {
					LOG.log(Level.FINEST, "SocketTimeoutException", e);
				} catch (IOException e) {
					LOG.log(Level.SEVERE, "Munin socket IOException", e);
				}
			}
		} finally {
			if (serverSocket != null && !serverSocket.isClosed()) {
				try {
					serverSocket.close();
				} catch (IOException e) {
					LOG.log(Level.SEVERE, e.getMessage(), e);
				}
			}
			socketStopped = true;
		}
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getAddress() {
		return address;
	}

	@Override
	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public Integer getPort() {
		return port;
	}

	@Override
	public void setPort(Integer port) {
		this.port = port;
	}

	@Override
	public Integer getTcpReadTimeOut() {
		return tcpReadTimeOut;
	}

	@Override
	public void setTcpReadTimeOut(Integer timeout) {
		this.tcpReadTimeOut = timeout;
	}

	@Override
	public Integer getMaxThreads() {
		return maxThreads;
	}

	@Override
	public void setMaxThreads(Integer maxThreads) {
		this.maxThreads = maxThreads;
	}

	@Override
	public Integer getCurrentThreads() {
		return currentThreads;
	}

}
