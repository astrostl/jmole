package net.welen.jmole;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import net.welen.jmole.protocols.Protocol;
import net.welen.jmole.protocols.munin.Munin;
import net.welen.jmole.protocols.nrpe.NRPE;
import net.welen.jmole.protocols.statsd.Statsd;
import net.welen.jmole.protocols.zabbix.Zabbix;

/**
 * A Java EE Singleton bean that is used to handle the lifecycle of JMole.
 * <p>
 * In a non Java EE environment it may be used a POJO to start and stop JMole.
 */
@Singleton
@Startup
public class Lifecycle {

	private final static Logger LOG = Logger.getLogger(Lifecycle.class.getName());
	
	private static JMole jmole;	
	private static Protocol[] protocols = { new Munin(), new NRPE(), new Zabbix(), new Statsd(), new net.welen.jmole.protocols.logger.Logger() };
	private static boolean running = false; 

	/**
	 * Starts up JMole and all activated protocols
	 */
	public static synchronized void setup() {
		if (running) {
			return;
		}
		running = true;
		
		LOG.log(Level.INFO, "Starting JMole");
		try {
			jmole = new JMole();
			jmole.register();
			jmole.configure();

			// Start protocols
			for (Protocol protocol : protocols) {
				if (protocol.isEnabled()) {				
					protocol.startProtocol(jmole);
				}
			}
				
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}

	/**
	 * Stops JMole and all activated protocols
	 */
	public static void cleanup() {
		LOG.log(Level.INFO, "Stopping JMole");
		try {
			// Stop protocols
			for (Protocol protocol : protocols) {
				if (protocol.isEnabled()) {				
					protocol.stopProtocol();
				}
			}						
			jmole.unregister();
			running = false;
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}

	@PostConstruct
	private void setupNotStatic() {
		setup();
	}

	@PreDestroy
	private void cleanupNotStatic() {
		cleanup();
	}
}
