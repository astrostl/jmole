package net.welen.jmole;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.management.MalformedObjectNameException;

import java.util.List;
import java.util.Map;
import java.io.FileNotFoundException;

import javax.management.MBeanException;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.ReflectionException;
import javax.management.IntrospectionException;

/**
 * MBean interface for JMole
 */
public interface JMoleMBean {

	/**
	 * Read and parses the config files and construct a new internal java
	 * representation. Also activates any thresholds present in that config
	 * 
	 * @throws MalformedObjectNameException
	 * @throws FileNotFoundException
	 * @throws MBeanException
	 * @throws AttributeNotFoundException
	 * @throws InstanceNotFoundException
	 * @throws ReflectionException
	 * @throws IntrospectionException
	 */
	public void configure() throws MalformedObjectNameException,
			FileNotFoundException, MBeanException, AttributeNotFoundException,
			InstanceNotFoundException, ReflectionException,
			IntrospectionException;

	/**
	 * Updates the list of all current matching MBeans
	 * 
	 * @throws MBeanException
	 * @throws AttributeNotFoundException
	 * @throws InstanceNotFoundException
	 * @throws ReflectionException
	 * @throws IntrospectionException
	 */
	public void discover() throws MBeanException, AttributeNotFoundException,
			InstanceNotFoundException, ReflectionException,
			IntrospectionException;

	/**
	 * Collects all measurements stated in the current configuration
	 * 
	 * @return A complex Map of data
	 * @throws InstanceNotFoundException
	 * @throws ReflectionException
	 * @throws MBeanException
	 * @throws AttributeNotFoundException
	 */
	public Map<String, List<Map<String, Map<String, Object>>>> collectMeasurements()
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	public String collectMeasurementsAsJSON()
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	/**
	 * Collects all measurements stated in the current configuration that
	 * matches the provided parameters
	 * 
	 * @param category The category  
	 * 
	 * @return A complex Map of data
	 * @throws InstanceNotFoundException
	 * @throws ReflectionException
	 * @throws MBeanException
	 * @throws AttributeNotFoundException
	 */
	public Map<String, Map<String, Object>> collectMeasurements(String category)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	public String collectMeasurementsAsJSON(String category)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	/**
	 * Collects all measurements stated in the current configuration that
	 * matches the provided parameters
	 * 
	 * @param category The category
	 * @param name The name of the measurement group  
	 * @return A complex Map of data
	 * 
	 * @throws InstanceNotFoundException
	 * @throws ReflectionException
	 * @throws MBeanException
	 * @throws AttributeNotFoundException
	 */
	public Map<String, Object> collectMeasurements(String category, String name)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	public String collectMeasurementsAsJSON(String category, String name)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	/**
	 * Collects all measurements stated in the current configuration that
	 * matches the provided parameters
	 * 
	 * @param category The category
	 * @param name The name of the measurement group
	 * @param attribute The attribute  
	 * @return A complex Map of data
	 * 
	 * @throws InstanceNotFoundException
	 * @throws ReflectionException
	 * @throws MBeanException
	 * @throws AttributeNotFoundException
	 */
	public Object collectMeasurement(String category, String name, String attribute)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	public String collectMeasurementAsJSON(String category, String name, String attribute)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	/**
	 * Get all currently triggered warning thresholds
	 * 
	 * @return The warnings
	 * @throws AttributeNotFoundException
	 * @throws InstanceNotFoundException
	 * @throws MBeanException
	 * @throws ReflectionException
	 */
	public Map<String, Map<String, String>> warningMessages() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException;
	public String warningMessagesAsJSON() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException;

	/**
	 * Get all currently triggered critical thresholds
	 * 
	 * @return The critical errors
	 * 
	 * @throws AttributeNotFoundException
	 * @throws InstanceNotFoundException
	 * @throws MBeanException
	 * @throws ReflectionException
	 */
	public Map<String, Map<String, String>> criticalMessages() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException;
	public String criticalMessagesAsJSON() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException;
	
}
