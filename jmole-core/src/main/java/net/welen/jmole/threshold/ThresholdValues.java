package net.welen.jmole.threshold;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

public class ThresholdValues {
	private String warningLowThreshold = "";
	private String warningHighThreshold = "";
	private String criticalLowThreshold = "";
	private String criticalHighThreshold = "";
	private String message = null;
	
	public String getWarningLowThreshold() {
		return warningLowThreshold;
	}
	public void setWarningLowThreshold(String warningLowThreshold) {
		this.warningLowThreshold = warningLowThreshold;
	}
	public String getWarningHighThreshold() {
		return warningHighThreshold;
	}
	public void setWarningHighThreshold(String warningHighThreshold) {
		this.warningHighThreshold = warningHighThreshold;
	}
	public String getCriticalLowThreshold() {
		return criticalLowThreshold;
	}
	public void setCriticalLowThreshold(String criticalLowThreshold) {
		this.criticalLowThreshold = criticalLowThreshold;
	}
	public String getCriticalHighThreshold() {
		return criticalHighThreshold;
	}
	public void setCriticalHighThreshold(String criticalHighThreshold) {
		this.criticalHighThreshold = criticalHighThreshold;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}	
}
