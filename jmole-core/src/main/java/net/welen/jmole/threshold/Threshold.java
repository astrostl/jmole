package net.welen.jmole.threshold;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Map;
import java.util.HashMap;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.InstanceNotFoundException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import net.welen.jmole.collector.MBeanCollector;
import net.welen.jmole.finder.MBeanFinder;

public class Threshold implements Runnable {
	
	private final static Logger LOG = Logger.getLogger(Threshold.class.getName());
		
	private boolean stopped = false;
	private int interval = 60000;
	
	private ThresholdValues thresholdValues = new ThresholdValues();
	private Map<String, ThresholdValues> individualThresholdValues = new HashMap<String, ThresholdValues> ();
	
	private Map<ObjectName, String> warningMessages = new HashMap<ObjectName, String>();
	private Map<ObjectName, String> criticalMessages = new HashMap<ObjectName, String>();

	private MBeanFinder mBeanFinder;
	private MBeanCollector mBeanCollector;
	private String attribute;
	private String message = "%s";
	
	@Override
	public void run() {
		stopped = false;
		while (!stopped) {		
			LOG.log(Level.FINE, "Running threshold check");

			Map<ObjectName, String> newWarningMessages = new HashMap<ObjectName, String>();
			Map<ObjectName, String> newCriticalMessages = new HashMap<ObjectName, String>();

			for (ObjectName objectName : mBeanFinder.getMatchingObjectNames()) {
				try {
					Object valueObject = mBeanCollector.getValues(objectName).get(attribute);
					if (valueObject == null) {
						LOG.log(Level.FINE, "Data collection returned null. Skipping it as it is probably not calculated yet");
						continue;
					}
					
					Double value = Double.parseDouble(valueObject.toString());
					ThresholdValues individualThresholdValue = individualThresholdValues.get(mBeanCollector.getConstructedName(objectName));
					String low;
					String high;
					
					String message = this.message;
					
					// Warning
					if (individualThresholdValue == null) {
						low = getWarningLowThreshold();	
						high = getWarningHighThreshold();
					} else {
						low = individualThresholdValue.getWarningLowThreshold();
						high = individualThresholdValue.getWarningHighThreshold();
						if (individualThresholdValue.getMessage() != null) {
							message = individualThresholdValue.getMessage();
						}
					}
					low = calculateThreshold(low, mBeanCollector, objectName, attribute);
					high = calculateThreshold(high, mBeanCollector, objectName, attribute);
					
					if (!low.isEmpty() && value < Double.parseDouble(low)) {
						newWarningMessages.put(objectName, constructMessage(message, attribute + ": " + value + " < " + low));
					}
					if (!high.isEmpty() && value > Double.parseDouble(high)) {
						newWarningMessages.put(objectName, constructMessage(message, attribute + ": " + value + " > " + high));
					}

					message = this.message;
					
					// Critical					
					if (individualThresholdValue == null) {
						low = getCriticalLowThreshold();	
						high = getCriticalHighThreshold();
					} else {
						low = individualThresholdValue.getCriticalLowThreshold();
						high = individualThresholdValue.getCriticalHighThreshold();
						if (individualThresholdValue.getMessage() != null) {
							message = individualThresholdValue.getMessage();
						}
					}
					low = calculateThreshold(low, mBeanCollector, objectName, attribute);
					high = calculateThreshold(high, mBeanCollector, objectName, attribute);
					
					if (!low.isEmpty() && value < Double.parseDouble(low)) {
						newCriticalMessages.put(objectName, constructMessage(message, attribute + ": " + value + " < " + low));
					}
					if (!high.isEmpty() && value > Double.parseDouble(high)) {
						newCriticalMessages.put(objectName, constructMessage(message, attribute + ": " + value + " > " + high));
					}
					
				} catch (Throwable t) {
					LOG.log(Level.SEVERE, t.getMessage(), t);
				}
			}
			warningMessages = newWarningMessages;
			criticalMessages = newCriticalMessages;
			
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				LOG.log(Level.WARNING, e.getMessage(), e);
			}
		}			
		LOG.log(Level.FINE, "Thread stopped");
	}
	
	private String constructMessage(String message, String problemText) {
		return String.format(message, problemText);
	}
	
	public void stopThread() {
		LOG.log(Level.FINE, "Stopping thread");
		stopped = true;
	}

	public String getCriticalLowThreshold() {
		return thresholdValues.getCriticalLowThreshold();
	}

	public void setCriticalLowThreshold(String criticalLowThreshold) {
		this.thresholdValues.setCriticalLowThreshold(criticalLowThreshold);
	}

	public String getCriticalHighThreshold() {
		return thresholdValues.getCriticalHighThreshold();
	}

	public void setCriticalHighThreshold(String criticalHighThreshold) {
		this.thresholdValues.setCriticalHighThreshold(criticalHighThreshold);
	}		

	public String getWarningLowThreshold() {
		return thresholdValues.getWarningLowThreshold();
	}

	public void setWarningLowThreshold(String warningLowThreshold) {
		this.thresholdValues.setWarningLowThreshold(warningLowThreshold);
	}

	public String getWarningHighThreshold() {
		return thresholdValues.getWarningHighThreshold();
	}

	public void setWarningHighThreshold(String warningHighThreshold) {
		this.thresholdValues.setWarningHighThreshold(warningHighThreshold);
	}		

	public void setIndividualThresholds(Map<String, ThresholdValues> individualThresholdValues) {
		this.individualThresholdValues = individualThresholdValues;
	}

	public Map<String, ThresholdValues> getIndividualThresholds() {
		return individualThresholdValues;
	}
	
	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public Map<ObjectName, String> getWarningMessages() {
		return warningMessages;
	}

	public void setWarningMessages(Map<ObjectName, String> warningMessages) {
		this.warningMessages = warningMessages;
	}

	public Map<ObjectName, String> getCriticalMessages() {
		return criticalMessages;
	}

	public void setCriticalMessages(Map<ObjectName, String> criticalMessages) {
		this.criticalMessages = criticalMessages;
	}
	
	public void setMBeanFinder(MBeanFinder mBeanFinder) {
		this.mBeanFinder = mBeanFinder;
	}
	
	public void setMBeanCollector(MBeanCollector mBeanCollector) {
		this.mBeanCollector = mBeanCollector;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	static public String calculateThreshold(String thresholdString, MBeanCollector mbeanCollector, ObjectName objectName, String attribute) throws InstanceNotFoundException, ReflectionException {		
		String data = thresholdString;
		Map<String, Object> values = mbeanCollector.getValues(objectName);
		LOG.log(Level.FINE, "Before variable replacement: " + data);
		for (String attributeName : mbeanCollector.getAttributes()) {			
			if (values.containsKey(attributeName) && values.get(attributeName) != null) {
				String tmpSplit[] = data.split(",");
				for (int i=0; i<tmpSplit.length; i++) {
					if (tmpSplit[i].equals(attributeName)) {
						tmpSplit[i] = values.get(attributeName).toString();
					}
				}
				StringBuffer tmp = new StringBuffer();
				for (String part : tmpSplit) {
					if (tmp.length() > 0) {
						tmp.append(",");
					}
					tmp.append(part);
				}
				data = tmp.toString();
			}
		}			
		LOG.log(Level.FINE, "After variable replacement: " + data);
		return rpnCalculate(data);
	}
	
	static private String rpnCalculate(String expr) {		
		if (expr == null || expr.isEmpty()) {
			return "";
		}
		Stack<Double> stack = new Stack<Double>();
		String pieces[] = expr.split(",");
		for (int i = 0; i < pieces.length; i++) {
			String value = pieces[i].trim();
			if (value.equals("+")) {
				Double lastValue = stack.pop();
				Double lastLastValue = stack.pop();
				stack.push(lastLastValue + lastValue);
			} else if (value.equals("-")) {
				Double lastValue = stack.pop();
				Double lastLastValue = stack.pop();
				stack.push(lastLastValue - lastValue);
			} else if (value.equals("*")) {
				Double lastValue = stack.pop();
				Double lastLastValue = stack.pop();
				stack.push(lastLastValue * lastValue);
			} else if (value.equals("/")) {
				Double lastValue = stack.pop();
				Double lastLastValue = stack.pop();
				stack.push(lastLastValue / lastValue);
			} else {
				stack.push(Double.valueOf(pieces[i]));
			}
		}
		double answer = stack.pop();
					
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		DecimalFormat formatter = new DecimalFormat("#.#", symbols); 
											
		return formatter.format(answer);
	}

}
