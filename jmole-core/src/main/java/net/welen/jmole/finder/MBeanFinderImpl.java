package net.welen.jmole.finder;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.ObjectName;
import javax.management.MBeanServer;
import javax.management.MBeanException;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.ReflectionException;
import javax.management.IntrospectionException;

import javax.management.MalformedObjectNameException;

import net.welen.jmole.Utils;

public class MBeanFinderImpl implements MBeanFinder {
	
	private final static Logger LOG = Logger.getLogger(MBeanFinderImpl.class.getName());
	
	private MBeanServer server = Utils.getMBeanServer();

	private Set<ObjectName> matchingObjectNames = new HashSet<ObjectName>();	
	private List<ObjectName> objectNameQueries = new ArrayList<ObjectName>();
	private String attributeName = null;
	private String attributeMatch = null;
	private String parameterName = null;
	private String parameterMatch = null;
	private String className = null;

	public Set<ObjectName> getMatchingObjectNames() {
		return matchingObjectNames;
	}

	public void updateMatchingObjectNames() throws MBeanException,
			AttributeNotFoundException, InstanceNotFoundException,
			ReflectionException, IntrospectionException {
				
		Set<ObjectName> objectNames = new HashSet<ObjectName>();
		for (ObjectName query : objectNameQueries) {
			objectNames.addAll(server.queryNames(query, null));
		}
		LOG.log(Level.FINE, "Found ObjectNames: " + objectNames);

		Set<ObjectName> filteredObjectNames = new HashSet<ObjectName>();
		for (ObjectName objectName : objectNames) {
			LOG.log(Level.FINE, "Checking ObjectName: " + objectName);
			if (!attributeMatch(objectName)) {
				LOG.log(Level.FINE, "Removing ObjectName: " + objectName);
				continue;
			}			
			if (!parameterMatch(objectName)) {
				LOG.log(Level.FINE, "Removing ObjectName: " + objectName);
				continue;
			}			
			if (!classNameCheck(objectName)) {
				LOG.log(Level.FINE, "Removing ObjectName: " + objectName);
				continue;
			}
			filteredObjectNames.add(objectName);
		}
		LOG.log(Level.FINE, "Updated ObjectNames: " + filteredObjectNames);
		matchingObjectNames = filteredObjectNames;
	}

	public void setObjectNameQueries(List<String> objectNameQuerys)
			throws MalformedObjectNameException {
		this.objectNameQueries = new ArrayList<ObjectName>();
		for (String query : objectNameQuerys) {		
			this.objectNameQueries.add(new ObjectName(query));
		}
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public void setAttributeMatch(String attributeMatch) {
		this.attributeMatch = attributeMatch;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public void setParameterMatch(String parameterMatch) {
		this.parameterMatch = parameterMatch;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	private boolean attributeMatch(ObjectName objectName)
			throws MBeanException, InstanceNotFoundException, ReflectionException {

		if (attributeName == null || attributeMatch == null) {
			return true;
		}

		try  {
			Object attribute = server.getAttribute(objectName, attributeName);
			return attribute.toString().matches(attributeMatch);
		} catch (AttributeNotFoundException e) {
			LOG.log(Level.FINE, "Attribute: " + attributeName + " not found for: " + objectName + ". Assuming it's not a match", e);
			return false;
		}
	}

	private boolean parameterMatch(ObjectName objectName)
			throws MBeanException, InstanceNotFoundException, ReflectionException {

		if (parameterName == null || parameterMatch == null) {
			return true;
		}

		String parameter = objectName.getKeyProperty(parameterName);	
		return parameter.matches(parameterMatch);
	}

	private boolean classNameCheck(ObjectName objectName)
			throws MBeanException, AttributeNotFoundException,
			InstanceNotFoundException, ReflectionException,
			IntrospectionException {

		if (className == null) {
			return true;
		}

		return server.getMBeanInfo(objectName).getClassName().equals(className);
	}
}
