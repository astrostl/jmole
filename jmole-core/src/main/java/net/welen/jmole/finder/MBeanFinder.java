package net.welen.jmole.finder;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2016 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Set;

import javax.management.ObjectName;
import javax.management.MBeanException;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.ReflectionException;
import javax.management.IntrospectionException;
import javax.management.MalformedObjectNameException;

public interface MBeanFinder {
	
	public Set<ObjectName> getMatchingObjectNames();
	
	public void updateMatchingObjectNames() throws MBeanException,
			AttributeNotFoundException, InstanceNotFoundException,
			ReflectionException, IntrospectionException;
	
	public void setObjectNameQueries(List<String> objectNameQueries) throws MalformedObjectNameException;
	
	public void setAttributeName(String attributeName);

	public void setAttributeMatch(String attributeMatch);

	public void setParameterName(String parameterName);

	public void setParameterMatch(String parameterMatch);

	public void setClassName(String className);
}
