#!/bin/sh

JMOLE_PARAMS='-Djmole.protocol.munin.enabled=true'

if test "$statsdEnabled" -eq 1
then
JMOLE_PARAMS='-Djmole.protocol.statsd.enabled=true'
fi

/opt/jboss/wildfly/bin/standalone.sh $JMOLE_PARAMS -c standalone-full-ha.xml
